/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import anotation.AttributAnotation;
import anotation.MethodeAnotation;
import anotation.NomtableAnotation;

/**
 *
 * @author Ivan
 */



@NomtableAnotation(nomtable="Dept")
public class Dept {
    @AttributAnotation(attribut="id")  
    Integer id;
    @AttributAnotation(attribut="name")  
    String name;

    public Dept(Integer id, String name) {
        

        this.id = id;
        this.name = name;
    }

    public Dept() {
        

    }
    
    
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @MethodeAnotation(methode="getalls" , page="test.jsp")
    public void  getall(){
    
    }
    @MethodeAnotation(methode="maka" , page="maka.jsp")
    public void  getOther(){
    
    }    
    
    
}
